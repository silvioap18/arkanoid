using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    public float speed = 100.0f;
    public AudioClip hit_sound;
    private AudioSource _as;

    void Start () {
        _as = GetComponent<AudioSource>();
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
    }
    
    float hitFactor(Vector2 ballPos, Vector2 racketPos,
        float racketWidth) {
        return (ballPos.x - racketPos.x) / racketWidth;
    }

    void OnCollisionEnter2D(Collision2D colision) {
        if (colision.gameObject.name == "racket"){
            float x = hitFactor(transform.position, 
                    colision.transform.position, 
                    colision.collider.bounds.size.x);
            Vector2 dir = new Vector2(x, 1).normalized;

            GetComponent<Rigidbody2D>().velocity = dir * speed;
        } else if(colision.gameObject.name == "block_blue"){
            _as.PlayOneShot(hit_sound);
        }
    }

}
